﻿using Contract;
using Contract.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientWPF
{
    /// <summary>
    /// Логика взаимодействия для Registration.xaml
    /// </summary>
    public partial class Registration : Window
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void CloseForm_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenUser();
            ViewModelRigistrationUser user = new ViewModelRigistrationUser() { Email = this.Email.Text, Password = this.Password.Password, PasswordConfirm = this.PasswordConfirm.Password };
            if (user.Password == user.PasswordConfirm)
            {
                if (clientService.UserProd.Registr(user))
                {
                    clientService.UserProd.ConfirmEmailReg(user.Email);
                    ConfirmEmail confirm = new ConfirmEmail();
                    confirm.Email = this.Email.Text;
                    confirm.Show();
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Пароли не совпадают!");
            }
        }
    }
}
