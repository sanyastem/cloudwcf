﻿using Contract;
using System.Windows;
using System.Windows.Input;

namespace ClientWPF
{
    /// <summary>
    /// Логика взаимодействия для ConfirmEmail.xaml
    /// </summary>
    public partial class ConfirmEmail
    {
        public string Email { get; set; }
        public ConfirmEmail()
        {
            InitializeComponent();
        }

        private void InCheck_Click(object sender, RoutedEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenUser();
            if (clientService.UserProd.ConfirmEmailIn(Email,int.Parse(this.Code.Text)))
            {
                MainWindow main = new MainWindow();
                main.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Неверный код");
            }
        }

        private void Rest_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenUser();
            clientService.UserProd.ConfirmEmailReg(Email);
        }
    }
}
