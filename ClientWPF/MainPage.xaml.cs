﻿using Contract;
using Contract.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ClientWPF
{
    public partial class MainPage : ICallBack
    {
        WindowState _prevState;
        public string State { get; set; }
        List<AllFileType> _allFiles;
        public string Email { get; set; }
        public string LeftState { get; set; }
        public MainPage() { }
        public MainPage(string email)
        {
            Email = email;
            State = email;
            InitializeComponent();
            Get(email);
           
        }
        static void fsw_Changed(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("Файл изменен!");
        }
        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
                Hide();
            else
                _prevState = WindowState;
        }
        private void TaskbarIcon_TrayLeftMouseDown(object sender, RoutedEventArgs e)
        {
            Show();
            WindowState = _prevState;
        }
        public void Get(string state="",bool flag = false)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);

            _allFiles?.Clear();
            if (state!="" && flag)
            {
                State = state;
            }
            _allFiles = clientService.FileProd.GetUserFileList("", State);
            clientService.FileProd.Watcher("", State);
            Element.Items.Clear();
            foreach (var item in _allFiles)
            {
                Element.Items.Add(item);
            }
            Element.Items.Refresh();
        }
        private void Shutdown_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }


        private void element_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            var obj = (ListView) sender;
            var type = _allFiles[obj.SelectedIndex].Type;
            var state = _allFiles[obj.SelectedIndex].State;
            string name = state + @"\" + _allFiles[obj.SelectedIndex].Name;
            if (type == "Папка")
            {
                LeftState = state;
                
                Get(name,true);
            }
            else
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter ="(*"+ name.Substring(name.LastIndexOf(".") - 1) + ")" +  "|*" + name.Substring(name.LastIndexOf(".")-1);
                saveFileDialog.FileName = _allFiles[obj.SelectedIndex].Name;
                if (saveFileDialog.ShowDialog() == true)
                {
                    using (var flstream = new BinaryWriter(saveFileDialog.OpenFile()))
                    {
                        flstream.Write(clientService.FileProd.DownloadFile(name));
                    }
                }
            }
            
        }

        private void btnLeft_Click(object sender, RoutedEventArgs e)
        {
            Get(LeftState,true);
            if (LeftState!=null || LeftState!="")
            {
                if (LeftState.LastIndexOf("\\")>-1)
                {
                    LeftState = LeftState.Substring(0, LeftState.LastIndexOf("\\"));
                }
                
            }
        }

        private void btnAddFolder_Click(object sender, RoutedEventArgs e)
        {
            AddFolder addFolder = new AddFolder(Email,State);
            addFolder.ShowDialog();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            List<FileDelModel> fileDelModel = new List<FileDelModel>();
            foreach (var item in Element.SelectedItems)
            {
                var al = (AllFileType)item;
                fileDelModel.Add(new FileDelModel() { Type = al.Type, Url = al.State + @"\" + al.Name });
            }
            clientService.FileProd.DeleteFile(fileDelModel.ToArray());
        }

        private void DowFile_Click(object sender, RoutedEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            OpenFileDialog openFileDialog = new OpenFileDialog();
           // openFileDialog.Filter ="All files(*.*) | *.* ";
            if (openFileDialog.ShowDialog() == true)
            {
                using (var flstream = new BinaryReader(openFileDialog.OpenFile()))
                {
                    clientService.FileProd.AddAndUpdateFile(Email,State, flstream.ReadBytes((int)openFileDialog.OpenFile().Length), openFileDialog.SafeFileName);
                }
            }
        }

        private void btnReName_Click(object sender, RoutedEventArgs e)
        {
            if (Element.SelectedIndex>-1)
            {
                Element.SelectedIndex.ToString();
                ReName reName = new ReName(State, _allFiles[Element.SelectedIndex].Name, _allFiles[Element.SelectedIndex].Type);
                reName.ShowDialog();
            }
        }

        private void checkBoxAuto_Checked(object sender, RoutedEventArgs e)
        {
        }

        private void openWindow_Click(object sender, RoutedEventArgs e)
        {
            Show();
        }

        private void CloseApp_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
