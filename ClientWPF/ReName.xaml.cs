﻿using Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientWPF
{
    /// <summary>
    /// Логика взаимодействия для ReName.xaml
    /// </summary>
    public partial class ReName : Window
    {
         string State { get; set; }
         string OldName { get; set; }
         string Type { get; set; }
        public ReName(string state, string oldName,  string type)
        {
            State = state;
            this.OldName = oldName;
            this.Type = type;
            InitializeComponent();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(new MainPage());
            clientService.FileProd.ReNameFile(State,this.OldName,Name.Text,this.Type);
            this.Close();
        }
    }
}
