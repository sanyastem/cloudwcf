﻿using Contract;
using Contract.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
        private void Registration(object sender, RoutedEventArgs e)
        {
            Registration registration = new Registration();
            registration.ShowDialog();
        }
        private void InUserAutoriseit(object sender, RoutedEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenUser();
            ViewModelLoginUser model = new ViewModelLoginUser() {Login = this.Login.Text,Password = this.Password.Password };
            if (clientService.UserProd.Login(model))
            {
                if (clientService.UserProd.ConfirmEmailOn(model))
                {
                    
                    MainPage main = new MainPage(this.Login.Text);
                    main.Show();
                    this.Close();
                }
                else
                {
                    clientService.UserProd.ConfirmEmailReg(model.Login);
                    ConfirmEmail confirm = new ConfirmEmail();
                    confirm.Email = this.Login.Text;
                    confirm.ShowDialog();
                    
                }
            }
            else
            {
                MessageBox.Show("Логин или пароль неверен!");
            }
        }
    }
}
