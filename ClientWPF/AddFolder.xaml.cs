﻿using Contract;
using System.Windows;

namespace ClientWPF
{
    /// <summary>
    /// Логика взаимодействия для AddFolder.xaml
    /// </summary>
    public partial class AddFolder
    {
        string State { get; set; }
        string Email { get; set; }
        public AddFolder(string email,string state)
        {
            State = state;
            Email = email;
            InitializeComponent();
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(new MainPage());
            clientService.FileProd.AddDirectory(Email,State,Name.Text);
            Close();
        }
    }
}
