﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    public class ClientService
    {
        ChannelFactory<IUser> _user;
        DuplexChannelFactory<IFile> _file;
        public IFile FileProd;
        public IUser UserProd;
        public void OpenUser()
        {
            _user = new ChannelFactory<IUser>(new WSDualHttpBinding(), new EndpointAddress("http://localhost:50798/User.svc"));
            UserProd = _user.CreateChannel();
        }
        public void OpenFile(object obj)
        {

                _file = new DuplexChannelFactory<IFile>(new InstanceContext(obj), "endpoint");
                FileProd = _file.CreateChannel();
            
        }

    }
}
