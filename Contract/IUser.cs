﻿using Contract.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract]
    public interface IUser
    {
        [OperationContract]
        bool Registr(ViewModelRigistrationUser user);
        [OperationContract]
        bool Login(ViewModelLoginUser user);
        [OperationContract]
        bool ConfirmEmailOn(ViewModelLoginUser user);
        [OperationContract]
        void ConfirmEmailReg(string email);
        [OperationContract]
        bool ConfirmEmailIn(string user, int code);
    }
}
