﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract.ViewModel
{
    public class FileDelModel
    {
        public string Url { get; set; }
        public string Type { get; set; }
    }
}
