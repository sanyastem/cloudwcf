﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contract.ViewModel
{
    public class AllFileType
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string State { get; set; }
        public bool StatusFile { get; set; }
    }
}
