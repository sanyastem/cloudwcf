﻿using System.ComponentModel.DataAnnotations;

namespace Contract.ViewModel
{ 
    public class ViewModelLoginUser
    {
        [Required]
        [EmailAddress]
        public string Login { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        public bool RememberMe { get; set; }
    }
}