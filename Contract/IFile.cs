﻿using Contract;
using Contract.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Contract
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(ICallBack))]
    public interface IFile
    {
        [OperationContract]
        List<AllFileType> GetUserFileList(string name, string state);
        [OperationContract]
        void AddAndUpdateFile(string userName,string getUrl, byte[] file, string name);
        [OperationContract]
        void DeleteFile(FileDelModel[] url);
        [OperationContract]
        void DeleteFileTrash(FileDelModel[] url, string name);
        [OperationContract]
        void ReNameFile(string state, string oldName, string newName, string type);
        [OperationContract]
        void AddDirectory(string userName, string url, string name);
        [OperationContract]
        byte[] DownloadFile(string urlFile);
        [OperationContract(IsOneWay =true)]
        void Watcher(string name, string state = "");
        [OperationContract]
        void RecoveryFileAndDirectory(FileDelModel[] url);
    }
    [ServiceContract]
    public interface ICallBack
    {
        [OperationContract(IsOneWay =true)]
        void Get(string state,bool flag = false);
    }
}
