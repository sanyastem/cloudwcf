﻿using System.ComponentModel.DataAnnotations;

namespace DB
{
   public class UserCloud
    {
        [Key]
        public int UserId { get; set; }
        public string Email { get; set; }
        public bool EmailConfirm { get; set; }
        public string PasswordHash { get; set; }
        public int Code { get; set; }
    }
}
