﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class UserDb 
    {
        string _connectionString = @"Persist Security Info=True;database=clouddb;server=localhost;user id=root;Password=admin";
        MySqlConnection _mySqlConnection;
        string _countUserStor = "CountUser";
        string _registrationUserStor = "RegistrationUser";
        string _updateCodeStor = "UpdateCode";
        string _autoresiteUserStor = "AutoresiteUser";
        string _confirmEmailIn = "ConfirmEmailIn";
        string _stateConfirmEmailStor = "StateConfirmEmail";
        string _addFileAndDirectory = "AddFileAndDirectory";
        string _updateFileAndDirectory = "UpdateFileAndDirectory";
        string _deleteFileAndDirectory = "DeleteFileAndDirectory";
        public UserDb()
        {
            _mySqlConnection = new MySqlConnection(_connectionString);
        }

        public int CountUser(string user)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_countUserStor, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            MySqlParameter ageParam = new MySqlParameter
            {
                ParameterName = "email",
                Value = user
            };
            command.Parameters.Add(ageParam);
            MySqlDataReader mySqlData = command.ExecuteReader();
            object sd = null;
            if (mySqlData.HasRows)
            {
                while (mySqlData.Read())
                {
                    sd = mySqlData.GetValue(0);
                    System.Diagnostics.Debug.Write(sd);
                }
            }
            _mySqlConnection.Close();
            return int.Parse(sd.ToString());
        }
        public bool RegistrationUser(string email, bool emailConfirm, string passwordHash)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_registrationUserStor, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            MySqlParameter ageParam =new MySqlParameter() { ParameterName = "email", Value = email };
            MySqlParameter ageParam1 = new MySqlParameter() { ParameterName = "emailconfirm", Value = emailConfirm };
            MySqlParameter ageParam2 = new MySqlParameter() { ParameterName = "passwordHash", Value = passwordHash };
            command.Parameters.Add(ageParam);
            command.Parameters.Add(ageParam1);
            command.Parameters.Add(ageParam2);
            int count = command.ExecuteNonQuery();
            _mySqlConnection.Close();
            return count > 0;
        }
        public bool UpdateCode(string email, int code)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_updateCodeStor, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            MySqlParameter[] ageParam = new MySqlParameter[]
            {
                new MySqlParameter(){ ParameterName = "email",Value = email},
                new MySqlParameter(){ ParameterName = "code",Value = code}
            };
            command.Parameters.AddRange(ageParam);
            var count = command.ExecuteNonQuery();
            _mySqlConnection.Close();
            return count > 0;
        }
        public bool AutoresiteUser(string email, string passwordHash)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_autoresiteUserStor, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            MySqlParameter[] ageParam = new MySqlParameter[]
            {
                new MySqlParameter(){ ParameterName = "email",Value = email},
                new MySqlParameter(){ ParameterName = "passwordHash",Value = passwordHash}
            };
            command.Parameters.AddRange(ageParam);
            MySqlDataReader mySqlData = command.ExecuteReader();
            object sd = null;
            if (mySqlData.HasRows)
            {
                while (mySqlData.Read())
                {
                    sd = mySqlData.GetValue(0);
                }
            }
            _mySqlConnection.Close();
            return int.Parse(sd.ToString()) > 0;
        }
        public bool StateConfirmEmail(string email)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_stateConfirmEmailStor, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            MySqlParameter ageParam = new MySqlParameter() { ParameterName = "email", Value = email };
            command.Parameters.Add(ageParam);
            MySqlDataReader mySqlData = command.ExecuteReader();
            object sd = null;
            if (mySqlData.HasRows)
            {
                while (mySqlData.Read())
                {
                    sd = mySqlData.GetValue(0);
                }
            }

            _mySqlConnection.Close();
            return int.Parse(sd.ToString())==0?true:false;
        }

        public bool ConfirmEmailIn(string user, int codeIn)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_confirmEmailIn, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            MySqlParameter email = new MySqlParameter() { ParameterName = "email", Value = user };
            MySqlParameter code = new MySqlParameter() { ParameterName = "code", Value = codeIn };

            command.Parameters.Add(email);
            command.Parameters.Add(code);

            int ext = command.ExecuteNonQuery();
            return ext > 0;
        }
        public bool AddFileAndDirectory(string user, string url)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_addFileAndDirectory, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            MySqlParameter email = new MySqlParameter() { ParameterName = "email", Value = user };
            MySqlParameter urlParameter = new MySqlParameter() { ParameterName = "url", Value = url.Replace(@"//", @"\") };

            command.Parameters.Add(email);
            command.Parameters.Add(urlParameter);
            int ext = command.ExecuteNonQuery();
            return ext>0;
        }
        public string UpdateFileAndDirectory(string url,string state,string urlOther="")
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_updateFileAndDirectory, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            MySqlParameter urlParameter = new MySqlParameter() { ParameterName = "url", Value = url.Replace(@"//", "\\") };
            MySqlParameter stateParameter = new MySqlParameter() { ParameterName = "state", Value = state };
            MySqlParameter urlOtherParameter = new MySqlParameter() { ParameterName = "urlOther", Value = urlOther.Replace(@"//", "\\") };

            command.Parameters.Add(urlParameter);
            command.Parameters.Add(stateParameter);
            command.Parameters.Add(urlOtherParameter);
            var read = command.ExecuteReader();
            object obj = null;
            while (read.Read())
            {
                obj = read.GetValue(0);
            }
            return obj.ToString();
        }
        public bool DeleteFileAndDirectory(string url)
        {
            _mySqlConnection.Open();
            MySqlCommand command = new MySqlCommand(_deleteFileAndDirectory, _mySqlConnection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            MySqlParameter urlParameter = new MySqlParameter() { ParameterName = "url", Value = url.Replace(@"//", "\\") };
            command.Parameters.Add(urlParameter);

            int ext = command.ExecuteNonQuery();
            return ext > 0;
        }
    }
}
