﻿using Contract;
using Contract.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Net.Mail;
using DB;

namespace BL
{
    public class UserLogic : IUser
    {
        private int GenerateNumber()
        {
            Random numberRand = new Random();
            string r = "";
            for (int i = 0; i < 5; i++)
            {
                r += numberRand.Next(0, 9).ToString();
            }
            return int.Parse(r);
        }
        private void SendMail(string email)
        {
            var code = GenerateNumber();
            var userDb = new UserDb();
            
                userDb.UpdateCode(email, code);

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("alexandr.rubis@gmail.com");
                mail.To.Add(new MailAddress(email));
                mail.Subject = "Код подтверждения";
                mail.Body = "Код подтверждения:"+ code;
                SmtpClient client = new SmtpClient();
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential("alexandr.rubis@gmail.com".Split('@')[0], "stem8114");
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);
            }
        }
        public bool ConfirmEmailOn(ViewModelLoginUser user)
        {
            var db = new UserDb();
            return db.StateConfirmEmail(user.Login);
          
                      
        }

        public void ConfirmEmailReg(string email)
        {
            SendMail(email);
        }

        public bool Login(ViewModelLoginUser user)
        {
            try
            {
                var db = new UserDb();
                
                    return db.AutoresiteUser(user.Login, ComputeHash(user.Password)); 
                 
            }
            catch (Exception)
            {

                return false;
            }
            
        }

        public bool Registr(ViewModelRigistrationUser user)
        {
            try
            {
                var db = new UserDb();
                
                    if (db.CountUser(user.Email) > 0)
                    {
                        return false;
                    }
                    db.RegistrationUser(user.Email, false, ComputeHash(user.Password));
                    return true;
                
                
            }
            catch (Exception)
            {
                return false;
            }
        }
        string ComputeHash(string pass)
        {
            byte[] checkSum = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(pass));
            return BitConverter.ToString(checkSum).Replace("-", String.Empty);
        }

        public bool ConfirmEmailIn(string user,int codeIn)
        {
            var db = new UserDb();
            if (db.ConfirmEmailIn(user,codeIn))
            {
                FileLogic file = new FileLogic();
                file.CreatMainFolder(user);
                return true;
            }
            return false;

        }
    }
}
