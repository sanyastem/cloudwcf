﻿using Contract;
using Contract.ViewModel;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using DB;


namespace BL
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class FileLogic : IFile
    {
        string _mainPatch = @"C:\Cloud";
        string _mainPatch_1 = @"C:\Cloud_1";
        string _mainPatch_2 = @"C:\Cloud_2";
        string _states;
        string _patch;
        string _patch_1;
        string _patch_2;
        ICallBack _callBack ;
        public void Watcher(string name, string state = "")
        {
            _callBack = OperationContext.Current.GetCallbackChannel<ICallBack>();
            _states = state == "" ? name : state;
            FileSystemWatcher fsw = new FileSystemWatcher(_mainPatch + @"\" + _states);
            fsw.Changed += OnChanged;
            fsw.Created += OnChanged;
            fsw.Deleted += OnChanged;
            fsw.Renamed += OnChanged;

            fsw.EnableRaisingEvents = true;

        }
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            _callBack.Get(_states);
        }
        public void AddAndUpdateFile(string userName,string getUrl, byte[] file, string name)
        {
            FileStream aFile = new FileStream(_mainPatch + @"\" + getUrl + @"\" + name, FileMode.Create);
            using (var flStream = new BinaryWriter(aFile))
            {
                aFile.Seek(0, SeekOrigin.End);
                flStream.Write(file);
                var db = new UserDb();
                if (!db.AddFileAndDirectory(userName,@"\" + getUrl + @"\" + name))
                {
                    throw new IOException();
                } 
            }
        }

        public void AddDirectory(string userName,string url, string name)
        {
            string patch = _mainPatch + @"\" + url;
            DirectoryInfo dir = new DirectoryInfo(patch);
            dir.CreateSubdirectory(name);
            var db = new UserDb();
            if (!db.AddFileAndDirectory(userName, @"\" + url + @"\"+name))
            {
                throw new IOException();
            }
        }

        public void DeleteFile(FileDelModel[] url)
        {
            var db = new UserDb();
            foreach (var item in url)
            {
                if (item.Type == "Папка")
                {
                    Directory.Delete(_mainPatch + @"\" + item.Url, true);
                    db.DeleteFileAndDirectory( @"\" + item.Url);
                }
                else
                {
                    File.Delete(_mainPatch + @"\" + item.Url);
                    db.DeleteFileAndDirectory( @"\" + item.Url);
                }

            }

        }

        public byte[] DownloadFile(string urlFile)
        {
            return File.ReadAllBytes(_mainPatch + @"\" + urlFile);
        }

        public List<AllFileType> GetUserFileList(string name, string state = "")
        {
            if (state == "")
            {
                _states = name;
            }
            else
            {
                _states = state;
            }

            _patch = _mainPatch + @"\" + _states;

            DirectoryInfo dir = new DirectoryInfo(_patch);
            List<AllFileType> allFile = new List<AllFileType>();
            foreach (var item in dir.GetDirectories())
            {
                allFile.Add(new AllFileType() { Name = item.Name, Date = item.LastWriteTime.ToString("D"), Time = item.LastWriteTime.ToString("T"), Type = "Папка", StatusFile = false, State = _states });
            }
            foreach (var item in dir.GetFiles())
            {
                allFile.Add(new AllFileType() { Name = item.Name, Date = item.LastWriteTime.ToString("D"), Time = item.LastWriteTime.ToString("T"), Type = "Файл", StatusFile = true, State = _states });
            }
            return allFile;
        }

        public void ReNameFile(string state, string oldName, string newName, string type)
        {
            if (type == "Файл")
            {
                var s = oldName.Substring(oldName.IndexOf("."));
                newName += s;
            }
            _patch = _mainPatch + @"\" + state.Replace(" ", string.Empty) + @"\" + oldName.Replace(" ", string.Empty);
            string newPatch = _mainPatch + @"\" + state.Replace(" ", string.Empty) + @"\" + newName.Replace(" ", string.Empty);
            Directory.Move(_patch, newPatch);
            var db = new UserDb();

            db.UpdateFileAndDirectory((@"\" + state.Replace(" ", string.Empty) + @"\" + oldName.Replace(" ", string.Empty)).Replace(@"\\", @"\"), "rename", (@"\" + state.Replace(" ", string.Empty) + @"\" + newName.Replace(" ", string.Empty)).Replace(@"\\", @"\"));
        }
        public void CreatMainFolder(string name)
        {
            DirectoryInfo dir = new DirectoryInfo(_mainPatch);
            if (!dir.Exists)
            {
                dir.Create();
            }
            dir.CreateSubdirectory(name);
            dir.CreateSubdirectory("Trash");
            DirectoryInfo dirTrash = new DirectoryInfo(_mainPatch+ @"\Trash");
            dirTrash.CreateSubdirectory(name);

        }

        public void DeleteFileTrash(FileDelModel[] url,string name)
        {
            var db = new UserDb();
            foreach (var item in url)
            {
                if (item.Type == "Папка")
                {
                    new DirectoryInfo(_mainPatch + @"\" + item.Url).MoveTo(_mainPatch + @"\Trash\" + item.Url);
                    db.UpdateFileAndDirectory( @"\" + item.Url, "delete",  @"\Trash\" + item.Url);
                }
                else
                {
                    new FileInfo(_mainPatch + @"\" + item.Url).MoveTo(_mainPatch + @"\Trash\" + name +@"\"+ item.Url.Substring(item.Url.LastIndexOf("//")+2));
                    db.UpdateFileAndDirectory( @"\" + item.Url, "delete", @"\Trash\"  + item.Url.Substring(item.Url.LastIndexOf("//") + 2));
                }

            }
        }
        public void RecoveryFileAndDirectory(FileDelModel[] url)
        {
            var db = new UserDb();
            foreach (var item in url)
            {
                if (item.Type == "Папка")
                {
                    new DirectoryInfo(_mainPatch + @"\" + item.Url).MoveTo(_mainPatch + db.UpdateFileAndDirectory(@"\" + item.Url, "recovery"));

                }
                else
                {
                    new FileInfo(_mainPatch + @"\" + item.Url).MoveTo(_mainPatch + db.UpdateFileAndDirectory(@"\" + item.Url, "recovery"));
                    
                }

            }
        }
    }
}
