﻿using Contract;
using Contract.ViewModel;
using System.Web.Mvc;
using System.Web.Security;

namespace ASPClient.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(ViewModelLoginUser model, string returnUrl)
        {
            ClientService clientService = new ClientService();
            clientService.OpenUser();
            if (ModelState.IsValid)
            {
                if (clientService.UserProd.Login(model))
                {
                    if (clientService.UserProd.ConfirmEmailOn(model))
                    {
                        FormsAuthentication.SetAuthCookie(model.Login, model.RememberMe);
                       
                        if (Url.IsLocalUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        clientService.UserProd.ConfirmEmailReg(model.Login);
                        return Redirect("~/Account/ConfirmEmail?email=" + model.Login);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }
            return View(model);
        }
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Registration()
        {
            return View();
        }
        [ValidateAjax]
        [HttpPost]
        public JsonResult Registration(ViewModelRigistrationUser user)
        {
            ClientService clientService = new ClientService();
            clientService.OpenUser();
            if (ModelState.IsValid)
            {
                if (clientService.UserProd.Registr(user))
                {
                    clientService.UserProd.ConfirmEmailReg(user.Email);
                    return Json(Url.Content("~/Account/ConfirmEmail?email=" + user.Email), JsonRequestBehavior.AllowGet);
                }
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false },JsonRequestBehavior.AllowGet);
        }
        public ActionResult ConfirmEmail(string email)
        {
            ViewBag.Email = email;

            return View();
        }
        [HttpPost]
        public ActionResult ConfirmEmail(string email,int code)
        {
            ClientService clientService = new ClientService();
            clientService.OpenUser();
            if (clientService.UserProd.ConfirmEmailIn(email,code))
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Con = "Неверный код";
                return View(email);
            }
            
        }
    }
}