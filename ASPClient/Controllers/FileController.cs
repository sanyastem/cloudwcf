﻿using ASPClient.Models;
using Contract;
using Contract.ViewModel;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace ASPClient.Controllers
{
    public class FileController : Controller, ICallBack
    {
        public void Get(string state, bool flag = true)
        {
            var context = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<GetHub>();
            context.Clients.All.displayMessage(state, flag);
        }
        // GET: File
        public JsonResult Index(string state = "", bool flag = false, string typeFolder = "")
        {


            ClientService clientService = new ClientService();
            List<AllFileType> json=null;
            clientService.OpenFile(this);
            //if (typeFolder == "delete")
            //{

            //}
            //else if (typeFolder == "SharedMe")
            //{

            //}
            //else if (typeFolder == "SharedUserMe")
            //{

            //}
            //else
            if (typeFolder == "Remove")
            {
                    json = clientService.FileProd.GetUserFileList(@"Trash\" + User.Identity.Name, state);
                    clientService.FileProd.Watcher(@"Trash\" + User.Identity.Name , state);   
            }
            else if(typeFolder == "MyFolder" || typeFolder=="")
            {
                if (state != "" && flag)
                {
                    json = clientService.FileProd.GetUserFileList("", state);
                    clientService.FileProd.Watcher("", state);
                }
                else
                {
                    json = clientService.FileProd.GetUserFileList(User.Identity.Name, state);
                    clientService.FileProd.Watcher(User.Identity.Name, state);
                }
            }
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AddDirectory(string name, string states)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            if (states == "")
            {
                clientService.FileProd.AddDirectory(User.Identity.Name, User.Identity.Name, name);
            }
            else
            {
                clientService.FileProd.AddDirectory(User.Identity.Name, states, name);
            }
            return Json(states, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ReName(string state, string oldName, string newName, string type)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            clientService.FileProd.ReNameFile(state, oldName, newName, type);
            return Json(state, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(FileDelModel[] filAllDel)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            clientService.FileProd.DeleteFile(filAllDel);
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteTrash(FileDelModel[] filAllDel)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            clientService.FileProd.DeleteFileTrash(filAllDel,User.Identity.Name);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RecoveryFile(FileDelModel[] filAllDel)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            clientService.FileProd.RecoveryFileAndDirectory(filAllDel);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public FileResult GetFile(string name, string urlFile)
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            var file = clientService.FileProd.DownloadFile(urlFile);
            string fileType = "application/octet-stream";
            string fileName = name;


            return File(file, fileType, fileName);
        }
        [HttpPost]
        public JsonResult Upload()
        {
            ClientService clientService = new ClientService();
            clientService.OpenFile(this);
            foreach (string file in Request.Files)
            {
                var upload = Request.Files[file];
                var state = Request.Form["state"];
                if (state == "")
                {
                    state = User.Identity.Name;
                }
                if (upload != null)
                {
                    string fileName = System.IO.Path.GetFileName(upload.FileName);

                    using (var binaryReader = new BinaryReader(upload.InputStream))
                    {
                        clientService.FileProd.AddAndUpdateFile(User.Identity.Name,state, binaryReader.ReadBytes(upload.ContentLength), upload.FileName);
                    }

                }
            }
            return Json("файл загружен");
        }


    }
}