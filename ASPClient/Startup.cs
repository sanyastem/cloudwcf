﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(ASPClient.Startup))]
namespace ASPClient
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}